package com.dicoding.javafundamental.getterAndSetter;

class Data {
    public int intPublic;
    private int intPrivate;
    public Data(){
        this.intPrivate = 0;
        this.intPublic = 0;
    }

    public int getIntPrivate() {
        return intPrivate;
    }

    public void setIntPrivate(int intPrivate) {
        this.intPrivate = intPrivate;
    }
}

class Lingkaran {
    private double diameter;

    Lingkaran(double diameter) {
        this.diameter = diameter;
    }
    // setter
    public void setJari2(double jari2) {
        this.diameter = jari2 * 2;
    }
    // getter;
    public double getJAri2() {
        return this.diameter/2;
    }
}

public class Main {
    public static void main(String[] args) {

        Data object = new Data();
        object.intPublic = 10; // write
        System.out.println(object.intPublic); // read
        // public let us to write or read. However the private unable us to read and write, we can use Getter and Setter instead

    }
}
