package com.dicoding.javafundamental.basic;

public class operators {
    public static void main(String[] args) {
//        int y, x;
//        x = 1;
//        y = ++x;
//        System.out.println(x);
//        System.out.println(y);

        int input = 2;
        int output = switchStatement(input);
        System.out.println(output);
        switchStatements();
    }
    static void conditional() {
        int value = 4;
        int anotherValue = 5;
        System.out.println("Conditional AND");
        boolean result = value == 3 && anotherValue == 5;
        boolean anotherResult = value != 3 && anotherValue == 5;
        System.out.println("Hasil Operator AND pada syarat value == 3 dan anotherValue == 5 adalah " + result);
        System.out.println("Hasil Operator AND pada syarat value != 3 dan anotherValue == 5 adalah " + anotherResult);
        System.out.println();
        System.out.println("Conditional OR");
        result = value == 3 || anotherValue == 5;
        anotherResult = value != 3 || anotherValue == 5;
        System.out.println("Hasil Operator OR pada syarat value == 3 dan anotherValue == 5 adalah " + result);
        System.out.println("Hasil Operator OR pada syarat value != 3 dan anotherValue == 5 adalah " + anotherResult);
        System.out.println();
    }
    static void equalityOperators() {
        int value = 5;
        int anotherValue = 4;
        boolean result;
        result = value == anotherValue;
        System.out.println("Hasil 'value == anotherValue' adalah " + result);
        System.out.println();
        System.out.println("Tidak sama dengan..");
        result = value != anotherValue;
        System.out.println("Hasil 'value != anotherValue' adalah " + result);
        System.out.println();
        System.out.println("Lebih besar dari..");
        result = value > anotherValue;
        System.out.println("Hasil 'value > anotherValue' adalah " + result);
        System.out.println();
        System.out.println("Sama atau lebih besar dari..");
        result = value >= anotherValue;
        System.out.println("Hasil 'value >= anotherValue' adalah " + result);
        System.out.println();
        System.out.println("Kurang dari..");
        result = value < anotherValue;
        System.out.println("Hasil 'value < anotherValue' adalah " + result);
        System.out.println();
        System.out.println("Sama atau kurang dari dengan..");
        result = value <= anotherValue;
        System.out.println("Hasil 'result <= anotherValue' adalah " + result);
        System.out.println();
    }
    static int switchStatement(int input) {
        int c = 0;
        switch (input) {
            case 1 :
                return ++c;
            default:
                /*

                * if we return --c, it means it will return the decement c's value, and vice versa

                */

                return --c;
        }
    }
    static void switchStatements() {
        int input = 1;
        char A = 'A';
        switch (A) {
            case 'A':
                ++input; // same as input++
            case 'b':
                input++;
            default:
                input--;
        }
        System.out.println(input);
    }
}
