package com.dicoding.javafundamental.classAndObject;

// player

class Player {
    private String name; // private means, its only can accessed by its sibling; read only for outside.
    private double health;
    private int level;
    private Weapon weapon;
    private Armor armor;

    Player(String name, double health) {
        this.name = name;
        this.health = health;
    }
    void equipWeapon(Weapon weapon) {
        this.weapon = weapon;
    }
    void equipArmor(Armor armor) {
        this.armor = armor;
    }
    void display( ) {
        System.out.println("Name   : " + this.name);
        System.out.println("Health : " + this.health + " hp");
        System.out.println("Weapon : " + this.weapon.getName() + " , power  : " + this.weapon.getAttackPower());
        System.out.println("Armor  : " + this.armor.getName() + " , defence : " + this.armor.getDefencePower());
    }

    void attack(Player opponent) {
        if (opponent.health <= 0) {
            System.out.println("Attacking denied because the opponent already dead");
        }else {
            System.out.println("\n" + this.name + " attacking " + opponent.name + " with power " + this.weapon.getAttackPower());
            opponent.defence(this.weapon.getAttackPower());
        }
    }

    void defence(double attackPower) {
        System.out.println(this.name + "'s " + "health is " + this.health);
        System.out.println(this.name + " gets damage " + attackPower);
        if (this.armor.getDefencePower() != 0) {
            double defencePower = this.armor.getDefencePower();
            System.out.println(this.name + "'s " + "defence power is " + defencePower);

            defencePower -= attackPower;
            System.out.println(this.name + "'s " + "defence power is " + defencePower);
            if (defencePower <= 0) {
                subtractHealth(attackPower);
            }
        }else if (this.armor.getDefencePower() <= 0) {
            subtractHealth(attackPower);
        }
    }
    void subtractHealth(double attackPower) {
        double defencePower = this.armor.getDefencePower();
        this.health -= attackPower;
        System.out.println( this.name + "'s health is " + this.health);
        if (this.health <= 0) {
            System.out.println(this.name + " is dead");
        }
    }
}

// weapon

class Weapon {
    private double attackPower;
    private String name;
    Weapon(String name, double attackPower) {
        this.name = name;
        this.attackPower = attackPower;
    }

    void display() {
        System.out.println("Weapon : " + this.name + " , power : " + this.attackPower);
    }

    public String getName() {
        return name;
    }

    public double getAttackPower() {
        return attackPower;
    }
}

// Armor

class Armor {
    private double defencePower;
    private String name;
    Armor(String name, double defencePower) {
        this.name = name;
        this.defencePower= defencePower;
    }
    void display() {
        System.out.println("Weapon : " + this.name + " , defence : " + this.defencePower);
    }

    public String getName() {
        return name;
    }

    public double getDefencePower() {
        return defencePower;
    }

    public void setDefencePower(double defencePower) {
        this.defencePower = defencePower;
    }
}

public class latihanKedua {
    public static void main(String[] args) {
        // make Player object
        Player player1 = new Player("Ucup", 100);
        Player player2 = new Player("Otong", 50);

        // make weapon object
        Weapon sword = new Weapon("diamond sword",15);
        Weapon slingshot = new Weapon("slingshot",100);

        // make armor object
        Armor ironArmor = new Armor("diamon armor",10);
        Armor tShirt = new Armor("T-Shirt", 0);
        // equip weapon and armor
        player1.equipWeapon(sword);
        player1.equipArmor(ironArmor);
        player2.equipArmor(tShirt);
        player2.equipWeapon(slingshot);

        player1.display();
        System.out.println("");
        player2.display();
//        player1.weapon.display();
//        player1.armor.display();

        System.out.println("");
        player1.attack(player2);
        player1.attack(player2);


        System.out.println("");
        player2.display();

        System.out.println("");
        player2.attack(player1);
        System.out.println("");
        player1.display();
        player2.attack(player1);
    }
}

