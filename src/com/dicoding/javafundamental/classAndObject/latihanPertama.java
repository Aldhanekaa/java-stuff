package com.dicoding.javafundamental.classAndObject;

class User {
    String userName;
    String name;
    UserFriends userFriends;

    User(String name, String userName, String arr) {
        this.name = name;
        this.userName = userName;
        this.userFriends = new UserFriends(arr);
    }
}

class UserFriends {
    String friends;
    UserFriends(String friends) {
        this.friends = friends;
    }

    public String getFriends() {
        return this.friends;
    }
}
public class latihanPertama {
    public static void main(String[] args) {
        User Aldhan = new User("Aldhan", "ALDHAN",  "ABC");
        System.out.println(Aldhan.userFriends.getFriends());
    }
}

//public class latihanPertama {
//    public static void main(String[] args) {
//
//    }
//}