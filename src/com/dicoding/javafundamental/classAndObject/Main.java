package com.dicoding.javafundamental.classAndObject;

// class without constructor
class Mahasiswa {
    String name;
    String fullName;
    String majors;
    String university;
}


class collegeStudent {
    String nickName;

    collegeStudent(String inputNickName) { // <= this is the constructor | this is same as constructor in JS
        nickName = inputNickName;
//        this.nickName = inputNickName; <= this is allowed, we can use this instead. this refer to this object

//        Main.test(); // <= not allowed because test function is a private method
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }
}


public class Main {
    public static void main(String[] args) {
        Mahasiswa Aldhan = new Mahasiswa();
        Aldhan.name = "Aldhaneka";
        Aldhan.majors = "Computer Science";
        Aldhan.university = "MIT";
        System.out.println("Aldhan object =======");
        System.out.println(Aldhan.name);

        if (Aldhan.fullName == null) {
            System.out.println("Full Name is null!");
        }

        int a = 1;
        int b = ++a;
        System.out.println(a);
        System.out.println(b);

        System.out.println("========== Class with constructor ===========");
        collegeStudent ALDHAN = new collegeStudent("Aldhan"); // <== this is object
        System.out.println(ALDHAN.nickName); // <== this is not allowed in OOP
        System.out.println(ALDHAN.getNickName()); // <== We use this instead
    }
    private static void test() {
        System.out.println("this is a test function");
    }
}
