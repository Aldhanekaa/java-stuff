package com.dicoding.javafundamental.dataType;

class lel {
    public static void main(java.lang.String[] args) {
        int nilaiInt = 450;
        System.out.println("nilai int:" + nilaiInt);

        long nilaiLong = nilaiInt;
        System.out.println(nilaiLong);

        byte nilaiByte = (byte) (nilaiInt + (byte) nilaiInt);
        System.out.println(nilaiByte);

//        =========================================================================================
        int a = 10;
        int b = 4;
        int c = a /b;
        System.out.println("int a = 10;\nint b = 4;\nint c = a\\b;\nc is: " + c); // will print integer type. Why? because we initialized c with int
        System.out.println("=======");
        //        =========================================================================================
        float C = a / b;
        System.out.println("int a = 10; \n" + "int b = 4; \n" + "float C = a / b; \n" + "C is: " + C); // will print 2, same as above. Because either a variable or b variable were not initialized with float data type
        System.out.println("=======");
        //        =========================================================================================
        float A = a;
        C = A / b;
        System.out.println("int a = 10;\nfloat A = a;\nint b = 4;\nfloat C = A \\ b\nC is: "+ C);
        System.out.println("=======");
        //        =========================================================================================
        float result = (float) a / b;
        System.out.println(result);
        System.out.println("=======");
        //        =========================================================================================

    }
}
