package com.dicoding.javafundamental.dataType;
import com.dicoding.javafundamental.dataType.User;

public class Main {
    public static void main(String[] args) {
        byte x = 127;
        int y = 2147483647;
        short a = 32767;
        System.out.println(x + y + a);
        long xya = x + y + a;
        System.out.println(xya * -1);
        float  $pi;
        $pi = 3.14f;
        System.out.println($pi);
        double $big$pi = 3.1415926535897932384626433f;
        System.out.println($big$pi);
        boolean benar, salah;
        benar = true;
        salah = false;

        if ($pi != $big$pi) {
            System.out.println(benar);
        }else {
            System.out.println(salah);
        }

        User Aldhan = new User();
        java.lang.String myName = Aldhan.ID("Aldhaneka Aufa Izzat");
        System.out.println(Aldhan);


    }
}
