package com.dicoding.javafundamental.dataType;
import  com.dicoding.javafundamental.dataType.User;
import com.dicoding.javafundamental.dataType.dataTypeConversion;

public class String {
    public static void main(java.lang.String[] args) {
        java.lang.String b, aldhanFullName;
        char[] aldhanName = {'A', 'L', 'D', 'H', 'A', 'N'};
        char[] saniyyahName = {'S', 'A', 'N', 'I', 'Y', 'Y', 'A', 'H'};
        java.lang.String aldhanNickName = new java.lang.String(aldhanName).toLowerCase();
        java.lang.String saniyyahNickName = new java.lang.String(saniyyahName);

        System.out.println("The last character of Aldhan's nickname is:");
        System.out.println(getTheLastCharacterOfString(aldhanNickName));

//        java.lang.String b = aldhanNickName.replace('a', 'b');
        System.out.println("Concat string:");
        System.out.println(aldhanNickName.concat(saniyyahNickName));

        System.out.println("Check if a string contains some character:");
        System.out.println(aldhanNickName.contains("a"));

        b = aldhanNickName.replace("a", "b");

//        System.out.println(aldhanNickName);

//        String benar = valueOf(true);

        // new instance from User class
        User Aldhaneka = new User();
        aldhanFullName = Aldhaneka.ID("Aldhaneka Aufa Izzat");
        System.out.println("Aldhan's Full Name is " + aldhanFullName + "\n\n");

        System.out.println("data type conversion ===============================");
        System.out.println("\n");
//        dataTypeConversion test = new dataTypeConversion();
//        test.main();
    }
    private static Character getTheLastCharacterOfString(java.lang.String name) {
//        System.out.println(name);
        int namesLength = name.length();
//        System.out.println(name.charAt(0));
        return name.charAt(namesLength-1);
    }
}

