package com.dicoding.javafundamental.Recursion;

public class Factorial {
    public static void main(String[] args) {
        int factorialOf5 = factorial(5);
        System.out.println(factorialOf5);
    }
    static int factorial(int x) {
        if (x <= 0) {
            return 1;
        }else {
            return x * factorial(x-1);
        }
    }
}
